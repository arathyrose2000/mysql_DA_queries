/*USING COMPANY DATABASE*/
USE COMPANY

-- 1 .   Print the names of all the employees who earn more than the average salary
SELECT Fname, Minit, Lname
FROM EMPLOYEE
WHERE Salary > (SELECT AVG(Salary)
FROM EMPLOYEE);

-- 2 .   List the names of employees who are supervisors but have no dependents
SELECT Fname, Minit, Lname
FROM EMPLOYEE
WHERE Ssn NOT IN (SELECT DISTINCT Essn
    FROM DEPENDENT) AND Ssn IN (SELECT DISTINCT Super_ssn
    FROM EMPLOYEE);

-- 3 .   Print the names of all the employees in decreasing order of their throughput of making money (throughput is defined by the amount they earn per hour of their work) (As calculated in Lab1)
SELECT Fname, Minit, Lname
FROM WORKS_ON W, EMPLOYEE E
WHERE Ssn = Essn
GROUP BY Essn
ORDER BY Salary/SUM(hours) DESC;

-- 4 .   Print the department name of the company in decreasing order of their average employee throughput
SELECT Dno, AVG(throughput)
FROM ( SELECT Essn, Salary/SUM(hours) as throughput
    FROM WORKS_ON W, EMPLOYEE E
    WHERE Ssn = Essn
    GROUP BY Essn ) AS A, EMPLOYEE E
WHERE A.Essn=E.Ssn
GROUP BY Dno
ORDER BY AVG(throughput) DESC;

-- 5 .   List the names of projects and the number of employees that work on it in decreasing order of employee count.
SELECT P.Pname, COUNT(*)
FROM WORKS_ON W, PROJECT P
WHERE P.Pnumber=W.Pno
GROUP BY P.Pnumber;

-- 6 .   For each department with more than 3 employees, retrieve the dept number and number of employees earning more than 37k.
SELECT Dno, count(*)
FROM (SELECT *
    FROM EMPLOYEE
    WHERE Salary > 37000) AS A
GROUP BY Dno
HAVING Dno in (SELECT Dno
from EMPLOYEE
GROUP BY Dno
HAVING count(*) > 3);

-- 7 .   Retrive employee names of all the employees not working on either project 1 or 2. (make sure output has no duplicate SSN) (assume (Fname, Minit, Lname) is unique for an employee)
SELECT DISTINCT Fname, Minit, Lname
FROM EMPLOYEE , WORKS_ON
WHERE Ssn=Essn AND Pno NOT IN (1,2);

/*USING COLLEGE DATABASE*/
USE COLLEGE;

-- 8 .   Retrieve the number of students who took the course Information Technology and their average CGPA.
SELECT COUNT(*), AVG(CGPA)
FROM STUDENT S , TAKES T, COURSE C
WHERE T.RollNo=S.RollNumber and T.CNO=C.CNO and CName="information technology";

-- 9 .   Retrieve the names of the faculty who is not head of a research centre.
SELECT distinct Name
from FACULTY
WHERE FID NOT IN (SELECT Head
FROM RESEARCH_CENTER);

-- 10.   Retrieve the course names where no student got an 'A' grade
SELECT Cname
FROM COURSE
WHERE CNO IN (SELECT CNo
FROM TAKES
GROUP BY CNo
HAVING MIN(GRADE) <> "A");