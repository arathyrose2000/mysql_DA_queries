# mysql_DA_queries

## Write SQL queries for the following.

### [SUBMISSION FORMAT] - team_name.sql

##### Q1-Q7 - COMPANY Database

##### Q8-Q10 - STUDENT Database

1.  Print the names of all the employees who earn more than the average salary
2.  List the names of employees who are supervisors but have no dependents
3.  Print the names of all the employees in decreasing order of their throughput of making money (throughput is defined by the amount they earn per hour of their work) (As calculated in Lab1)
4.  Print the department name of the company in decreasing order of their average employee throughput
5.  List the names of projects and the number of employees that work on it in decreasing order of employee count.
6.  For each department with more than 3 employees, retrieve the dept number and number of employees earning more than 37k.
7.  Retrive employee names of all the employees not working on either project 1 or 2. (make sure output has no duplicate SSN) (assume (Fname, Minit, Lname) is unique for an employee)
8.  Retrieve the number of students who took the course Information Technology and their average CGPA.
9.  Retrieve the names of the faculty who is not head of a research centre.
10. Retrieve the course names where no student got an 'A' grade.